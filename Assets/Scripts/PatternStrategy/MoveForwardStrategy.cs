﻿using UnityEngine;

namespace StrategyLab
{
    public class MoveForwardStrategy : IStrategy
    {
        private float speed;
        public MoveForwardStrategy(float _speed) => speed = _speed;
        
        public void Perform(Transform transform) => transform.Translate(Vector3.up * speed * Time.deltaTime);
    }
}