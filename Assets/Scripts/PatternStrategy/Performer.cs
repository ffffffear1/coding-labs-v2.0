﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrategyLab;
using UnityEngine;

public class Performer : MonoBehaviour
{
    private IStrategy curStrategy;
    
    public void SetStrategy(IStrategy strategy) => curStrategy = strategy;

    public void Move(float speed) => SetStrategy(new MoveForwardStrategy(speed));
    public void Rotate(float speedRotate) => SetStrategy(new RotateStrategy(speedRotate));
    public void Emmit(int countShots) => SetStrategy(new EmmitStrategy(countShots));

    private void Update()
    {
        curStrategy?.Perform(transform);   
    }
}
