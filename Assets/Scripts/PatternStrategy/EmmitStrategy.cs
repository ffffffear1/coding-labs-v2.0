﻿using UnityEngine;

namespace StrategyLab
{
    public class EmmitStrategy : IStrategy
    {
        private int countShots;
        public EmmitStrategy(int _countShots) => countShots = _countShots;

        public void Perform(Transform transform)
        {
            if(countShots <= 0) return;
            countShots--;
            transform.GetChild(0).GetComponent<ParticleSystem>().Play(); 
        }
    }
}