﻿using UnityEngine;

namespace StrategyLab
{
    public class RotateStrategy : IStrategy
    {
        private float speedRotate;
        public RotateStrategy(float _speedRotate) => speedRotate = _speedRotate;

        public void Perform(Transform transform) => transform.Rotate(new Vector3(0,0, -speedRotate));
    }
}