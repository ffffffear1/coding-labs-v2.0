﻿using UnityEngine;

namespace Command.Commands
{
    public class MoveCommand : ICommand
    {
        private Move movement;
        private Transform transform;
        private float dist;

        public MoveCommand(Transform transform, float dist)
        {
            movement = new Move();
            this.transform = transform;
            this.dist = dist;
        }
        
        public void Invoke() => movement.MoveForward(transform, dist);
        public void Undo() => movement.MoveForward(transform, dist * -1);
    }

    public class Move
    {
        public void MoveForward(Transform transform, float dist) => transform.Translate(Vector2.up * dist);
    }
}