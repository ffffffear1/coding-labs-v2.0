﻿using UnityEngine;

namespace Command.Commands
{
    public class RotateCommand : ICommand
    {
        private Rotate rotation;
        private Transform transform;
        private float angle;

        public RotateCommand(Transform transform, float angle)
        {
            rotation = new Rotate();
            this.transform = transform;
            this.angle = angle;
        }

        public void Invoke() => rotation.RotateObject(transform, angle);
        public void Undo() => rotation.RotateObject(transform,  angle * -1);
    }

    public class Rotate
    {
        public void RotateObject(Transform transform, float angle) => transform.Rotate(new Vector3(0,0,angle));
    }
}