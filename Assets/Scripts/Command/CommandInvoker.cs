﻿using System.Collections.Generic;

namespace Command
{
    public class CommandInvoker
    {
        private List<ICommand> commandsList = new List<ICommand>();
        private List<ICommand> commandsHistory = new List<ICommand>();

        public void AddCommand(ICommand command) => commandsList.Add(command);

        public void ProcessAll()
        {
            if (commandsList.Count.Equals(0)) return;

            foreach (var command in commandsList) { StartCommand(command); }
            
            ClearQueue();
        }

        public void Process()
        {
            if (commandsList.Count.Equals(0)) return;
        
            StartCommand(commandsList[0]);
            commandsList.RemoveAt(0);
        }

        public void Undo()
        {
            if (commandsHistory.Count.Equals(0)) return;
            
            commandsHistory[commandsHistory.Count - 1].Undo();
            commandsHistory.RemoveAt(commandsHistory.Count - 1);
        }
        
        private void StartCommand(ICommand command)
        {
            command.Invoke();
            commandsHistory.Add(command);
        }

        private void ClearQueue() => commandsList = new List<ICommand>();
    }
}