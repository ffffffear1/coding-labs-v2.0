﻿using Command.Commands;
using UnityEngine;

namespace Command
{
    public class CommandController : MonoBehaviour
    {
        private CommandInvoker commandInvoker;
        private MoveCommand move;
        private RotateCommand right;
        private RotateCommand left;
        
        private void Start()
        {
            move = new MoveCommand(transform, 1);
            right = new RotateCommand(transform, -90);
            left = new RotateCommand(transform, 90);
            
            commandInvoker = new CommandInvoker();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.W))
                commandInvoker.AddCommand(move);
            
            if (Input.GetKeyDown(KeyCode.D))
                commandInvoker.AddCommand(right);
            else if (Input.GetKeyDown(KeyCode.A))
                commandInvoker.AddCommand(left);

            if (Input.GetKeyDown(KeyCode.Return))
                commandInvoker.ProcessAll();
            
            if (Input.GetKeyDown(KeyCode.Space))
                commandInvoker.Process();
            
            if (Input.GetKeyDown(KeyCode.Backspace))
                commandInvoker.Undo();
        }
    }
}