﻿namespace Command
{
    public interface  ICommand
    {
        // public string Description;
        
        void Invoke();
        void Undo();    
    }
}
