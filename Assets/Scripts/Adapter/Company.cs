﻿using UnityEngine;

namespace Adapter
{
    public class Company : MonoBehaviour
    {
        private IWorker worker;

        private void Start()
        {
            worker = new BuilderAdapter(new Builder());
            worker.Work();
            worker.PayMoney(15);
        }
    }
}