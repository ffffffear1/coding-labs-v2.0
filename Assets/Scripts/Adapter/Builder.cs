﻿using UnityEngine;

namespace Adapter
{
    public class Builder
    {
        public void Build() => Debug.Log("Строим дом...");
        public void GetMoney(Salary salary) => Debug.Log($"Получено {salary.Money} деняг");
    }
}