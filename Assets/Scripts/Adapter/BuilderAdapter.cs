﻿namespace Adapter
{
    public class BuilderAdapter : IWorker
    {
        private Builder builder;

        public BuilderAdapter(Builder builder)
        {
            this.builder = builder;
        }
        
        public void Work() => builder.Build();
        
        public void PayMoney(int v)
        {
            var salary = new Salary {Money = v};
            builder.GetMoney(salary);
        }
    }
}