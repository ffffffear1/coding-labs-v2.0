﻿namespace Decorator
{
    public interface IDebugable
    {
        void Show(string s);
    }
}