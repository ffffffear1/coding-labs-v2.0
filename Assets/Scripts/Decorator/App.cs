﻿using System;
using UnityEngine;

namespace Decorator
{
    public class App : MonoBehaviour
    {
        private void Start()
        {
            IDebugable debugable = new CustomDebuger();
            debugable = new ConcreteCustomDebugerDecorator1(debugable);
            debugable = new ConcreteCustomDebugerDecorator2(debugable);
            debugable.Show("Plusplus");
        }
    }
}