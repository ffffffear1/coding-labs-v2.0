﻿using System;
using UnityEngine;

namespace Decorator
{
    public class CustomDebuger : IDebugable
    {
        public void Show(string s)
        {
            Debug.Log(s);
        }
    }
}