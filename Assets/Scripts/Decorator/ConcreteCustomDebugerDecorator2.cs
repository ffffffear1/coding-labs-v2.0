﻿using System;
using System.IO;
using UnityEngine;

namespace Decorator
{
    public class ConcreteCustomDebugerDecorator2 : CustomDebugerDecorator
    {
        private string Path = "Assets/Scripts/Decorator/Text/Info.txt";
        
        public ConcreteCustomDebugerDecorator2(IDebugable debugable) : base(debugable)
        {
        }
        
        public override void Show(string s)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(Path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(s);
                }
                Console.WriteLine("Запись есть");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            base.Show(s);
        }
    }
}