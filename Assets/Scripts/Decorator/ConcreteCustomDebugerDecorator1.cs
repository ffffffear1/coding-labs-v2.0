﻿using UnityEngine;

namespace Decorator
{
    public class ConcreteCustomDebugerDecorator1 : CustomDebugerDecorator
    {
        public ConcreteCustomDebugerDecorator1(IDebugable debugable) : base(debugable)
        {
        }
        
        
        public override void Show(string s)
        {
            Debug.Log($"Length: {s.Length}");
            base.Show(s);
        }
    }
}