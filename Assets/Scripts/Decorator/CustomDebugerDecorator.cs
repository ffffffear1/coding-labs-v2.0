﻿namespace Decorator
{
    public class CustomDebugerDecorator : IDebugable
    {
        private IDebugable debugable;

        public CustomDebugerDecorator(IDebugable debugable)
        {
            this.debugable = debugable;
        }
        
        public void SetCustomDebugerDecorator(IDebugable debugable)
        {
            this.debugable = debugable;
        }

        public virtual void Show(string s)
        {
            debugable?.Show(s);
        }
    }
}