﻿using UnityEngine;

namespace PrototypeAndCloning
{
    public class College : MonoBehaviour
    {
        private Student orig;
        private Student clone;
        
        private void Start()
        {
            orig = new Student("Igor", "Babyshkin", 18, new Group(1, "Code"));
            clone = (Student) orig.DeepCopy();
            clone.firstName = "Vasya";
            clone.lastName = "Pupkin";
            clone.group.course++;
            
            Debug.Log($"orig: {orig.firstName} {orig.lastName}, {orig.group.course} course");
            Debug.Log($"clone: {clone.firstName} {clone.lastName}, {clone.group.course} clone");
        }
    }
}