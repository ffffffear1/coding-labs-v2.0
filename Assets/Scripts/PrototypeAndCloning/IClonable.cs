﻿namespace PrototypeAndCloning
{
    public interface IClonable
    {
        object ShallowCopy();
        object DeepCopy();
    }
}