﻿using PrototypeAndCloning;

public class Student :  IClonable
{
    public string firstName;
    public string lastName;
    public int age;
    public Group group;

    public Student(string firstName, string lastName, int age, Group group)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.group = group;
    }

    public object ShallowCopy()
    {
        return (Student) MemberwiseClone();
    }

    public object DeepCopy()
    {
        var group = new Group(this.group.course, this.group.spec);
        return new Student(this.firstName, this.lastName, this.age, group);
    }
}

public class Group
{
    public int course;    
    public string spec;

    public Group(int course, string spec)
    {
        this.course = course;
        this.spec = spec;
    }
}
