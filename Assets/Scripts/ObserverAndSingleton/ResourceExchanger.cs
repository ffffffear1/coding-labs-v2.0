﻿using UnityEngine;

public class ResourceExchanger : MonoBehaviour
{
    private ResourceBank resourceBank;
    
    void Start()
    {
        resourceBank = ResourceBank.instance;
        resourceBank.OnResChanged += CheckProgress;
    }

    private void CheckProgress(ResType arg1, int arg2)
    {
        if(resourceBank.GetResource(ResType.Wood) >= 10)
        {
            resourceBank.dictionary[ResType.House] += 1;  
            resourceBank.dictionary[ResType.Wood] -= 10;  
        }

        if (resourceBank.GetResource(ResType.House) >= 5)
        {
            var countHouse = Random.Range(1, 5);
            
            resourceBank.dictionary[ResType.House] -= countHouse;  
            resourceBank.dictionary[ResType.Money] += countHouse * 100;  
        }

        if (resourceBank.GetResource(ResType.Money) >= 1000)
        {
            var countWorkers = Random.Range(1, 4);
            
            resourceBank.dictionary[ResType.Worker] += countWorkers;  
            resourceBank.dictionary[ResType.Money] -= countWorkers * 250;  
        }
    }
}
