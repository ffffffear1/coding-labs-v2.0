﻿using UnityEngine;
using UnityEngine.UI;

public class ResoursesObserver : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private ResType type;
    
    private void Start() => ResourceBank.instance.OnResChanged += UpdateData;
    private void UpdateData(ResType res, int val) => text.text = ResourceBank.instance.GetResource(type).ToString();

}
