﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class ResourceBank : MonoBehaviour
{
    public  Action<ResType, int> OnResChanged = (r, v) => { };

    public Dictionary<ResType, int> dictionary = new Dictionary<ResType, int>(4);
    
    public static ResourceBank instance;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);
    }

    private void Start()
    {
        dictionary.Add(ResType.Worker, 2);
        dictionary.Add(ResType.Wood, 0);
        dictionary.Add(ResType.House, 0);
        dictionary.Add(ResType.Money, 0);

        StartCoroutine(StartWorking());
    }

    private IEnumerator StartWorking()
    {
        while (true)
        {
            ChangeResource(ResType.Wood, GetResource(ResType.Worker));
            yield return new WaitForSeconds(0.5f);
        }
    }
    
    public void ChangeResource(ResType res, int value)
    {
        dictionary[res] += value;
        OnResChanged(res, GetResource(res));
    }

    public int GetResource(ResType res)
    {
        return dictionary[res];
    }
}

public enum ResType
{
    Worker,
    Wood,
    House,
    Money
}
