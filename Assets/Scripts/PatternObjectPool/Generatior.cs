﻿using System;
using System.Collections;
using System.Collections.Generic;
using PatternObjectPool;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PatternObjectPool
{
    public class Generatior : MonoBehaviour
    {
        [SerializeField] private Transform[] pathPoints;

        private void Start() => StartCoroutine(Spawn());


        private IEnumerator Spawn()
        {
            while (true)
            {
                GameObject enemy = PoolManager.GetObject("BoxEnemy",
                    pathPoints[Random.Range(0, pathPoints.Length)].position,
                    Quaternion.identity);
                if (enemy != null)
                    enemy.GetComponent<Rigidbody2D>().velocity = Vector2.down * 5;
                yield return new WaitForSeconds(Random.Range(1, 4));
            }
        }
    }
}
