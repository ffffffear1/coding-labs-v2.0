﻿using System.Collections.Generic;
using UnityEngine;

namespace PatternObjectPool
{
    public class ObjectPooling
    {
        private List<PoolObject> listObjects;
        private Transform parentTransform;

        public void InIt(int count, PoolObject sample, Transform parent)
        {
            listObjects = new List<PoolObject>();
            parentTransform = parent;

            for (int i = 0; i < count; i++)
            {
                Add(sample, parent);
            }
        }

        public PoolObject GetObject()
        {
            for (int i = 0; i < listObjects.Count; i++)
            {
                if (!listObjects[i].gameObject.activeSelf)
                {
                    listObjects[i].gameObject.SetActive(true);
                    return listObjects[i];
                }
            }

            Debug.Log("Empty");
            return null;
        }
        
        public void Add(PoolObject sample, Transform parent)
        {
            var newObject = GameObject.Instantiate(sample.gameObject);
            newObject.name = sample.name;
            newObject.transform.SetParent(parent);
            listObjects.Add(newObject.GetComponent<PoolObject>());
            newObject.SetActive(false);
        }
    }
}