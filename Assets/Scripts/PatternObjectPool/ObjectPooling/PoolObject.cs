﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PatternObjectPool
{
    public class PoolObject : MonoBehaviour
    {
        private Vector2 min, max;

        void Start()
        {
            min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
            max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        }


        public void ReturnToPool() => gameObject.SetActive(false);

        private void Update()
        {
            if (transform.position.y > max.y || transform.position.y < min.y)
            {
                if (gameObject.CompareTag("BoxEnemy"))
                {
                    GameManager.instance.SetFine(1);
                }
                ReturnToPool();
            }
        }
    }
}
