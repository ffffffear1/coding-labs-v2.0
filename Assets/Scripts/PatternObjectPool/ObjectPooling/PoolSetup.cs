﻿using System;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace PatternObjectPool
{
    public class PoolSetup : MonoBehaviour
    {
        [SerializeField] private PoolManager.PoolPart[] pools;

        private void Awake()
        {
            InIt();
        }
        
        private void InIt () {
            PoolManager.InIt(pools);
        }

        private void OnValidate()
        {
            for (int i = 0; i < pools.Length; i++)
            {
                pools[i].name = pools[i].prefab.name;
            }
        }
    }
}