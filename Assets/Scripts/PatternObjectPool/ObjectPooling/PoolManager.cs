﻿using UnityEngine;

namespace PatternObjectPool
{
    public static class PoolManager
    {
        private static PoolPart[] pools;
        private static GameObject parent;
        
    
        public static void InIt(PoolPart[] newPools)
        {
            pools = newPools;
            parent = new GameObject();
            parent.name = "pool";

            for (int i = 0; i < pools.Length; i++)
            {
                if (pools[i].prefab != null)
                {
                    pools[i].ferula = new ObjectPooling();
                    pools[i].ferula.InIt(pools[i].count, pools[i].prefab, parent.transform);
                }
            }
        }

        public static GameObject GetObject(string name, Vector2 position, Quaternion rotation)
        {
            GameObject result;
            if (pools != null)
            {
                for (int i = 0; i < pools.Length; i++)
                {
                    if (string.Compare(pools[i].name, name).Equals(0))
                    {
                        var newObj = pools[i].ferula.GetObject();
                        if (newObj != null)
                        {
                            result = newObj.gameObject;
                            result.transform.position = position;
                            result.transform.rotation = rotation;
                            return result;
                        }
                    }
                }
            }
            return null;
        }
        
        
        [System.Serializable]
        public struct PoolPart
        {
            public string name;
            public PoolObject prefab;
            public int count;
            public ObjectPooling ferula;
        }
    }
}