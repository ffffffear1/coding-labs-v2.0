﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PatternObjectPool
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Text startGameText;
        [SerializeField] private Text stopGameText;

        public static GameManager instance;
        public StateGame
         state;


        private Action<StateGame> onStateChanged;
        
        private int fine = 0;

        private void Awake()
        {
            onStateChanged += CheckPos;
            SetState(StateGame.Start);
            instance = this;
        }

        private void Update()
        {
            if (Input.anyKey && state.Equals(StateGame.Start))
            {
                StartGame();
            }
            if (Input.anyKey && state.Equals(StateGame.Death))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        public void StartGame()
        {
            SetState(StateGame.Gameplay);
        }
        
        public void StopGame()
        {
            SetState(StateGame.Death);
        }

        private void CheckPos(StateGame state)
        {
            switch (state)
            {
                case StateGame.Start:
                    startGameText.gameObject.SetActive(true);
                    Time.timeScale = 0;
                    break;
                case StateGame.Gameplay:
                    startGameText.gameObject.SetActive(false);
                    Time.timeScale = 1;
                    break;
                case StateGame.Death:
                    stopGameText.gameObject.SetActive(true);
                    Time.timeScale = 0;
                    break;
            }
        }
        
        public void SetFine(int add)
        {
            set:
            {
                fine += add;
                CheckWin();
            }
        }

        private void CheckWin()
        {
            if (fine >= 3)
            {
                StopGame();
            }
        }

        public void SetState(StateGame newState)
        {
            set:
            {
                state = newState;
                onStateChanged(state);
            }
        }
    }
    
    public enum StateGame
    
    {
        Start,
        Gameplay,
        Death
    }
}