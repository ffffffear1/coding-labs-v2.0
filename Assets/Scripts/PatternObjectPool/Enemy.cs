﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PatternObjectPool
{
    public class Enemy : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Bullet"))
            {
                other.GetComponent<PoolObject>().ReturnToPool();
                GetComponent<PoolObject>().ReturnToPool();
            }
        }
    }
}