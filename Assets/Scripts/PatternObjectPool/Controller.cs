﻿using System.Collections;
using System.Collections.Generic;
using PatternObjectPool;
using UnityEngine;

namespace PatternObjectPool
{
    public class Controller : MonoBehaviour
    {
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                GameObject bullet = PoolManager.GetObject("Bullet", Camera.main.ScreenToWorldPoint(Input.mousePosition),
                    Quaternion.identity);
                if (bullet != null)
                    bullet.GetComponent<Rigidbody2D>().velocity = Vector2.up * 10;
            }
        }
    }
}