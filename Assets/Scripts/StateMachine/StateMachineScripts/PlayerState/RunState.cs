﻿using UnityEngine;

namespace StateMachine.StateMachineScripts.PlayerState
{
    public class RunStates : State
    {
        public RunStates(StateMachine stateMachine, Character character) : base(stateMachine, character) { }

        public override void Enter()
        {
            base.Enter();
            character.agent.speed = 2;        
        }

        public override void HandleInput()
        {
            base.HandleInput();
            
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                    character.target = hit.point;
            }
            character.Move(character.target);
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (Vector3.Distance(character.transform.position, character.target) < 0.5f)
                stateMachine.ChangeState(character.idle);
        }

        public override void Exit()
        {
            base.Exit();
            character.SetAnimationBool("isRun", false);
        }
    }
}