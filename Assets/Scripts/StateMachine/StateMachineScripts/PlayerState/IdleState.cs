﻿using UnityEngine;

namespace StateMachine.StateMachineScripts.PlayerState
{
    public class IdleState : State
    {
        public IdleState(StateMachine stateMachine, Character character) : base(stateMachine, character) { }
        
        public override void Enter()
        {
            base.Enter();
        }
        
        public override void LogicUpdate()
        {
            base.LogicUpdate();

            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    character.target = hit.point;
                    stateMachine.ChangeState(character.run);
                }
            }
        }
        
        public override void Exit()
        {
            base.Exit();
            character.SetAnimationBool("isRun", true);
        }    
    }
}