﻿﻿namespace StateMachine.StateMachineScripts
{
    public class StateMachine
    {
        public State curState { get; private set; }

        public void Init(State startState)
        {
            curState = startState;
            curState.Enter();
        }

        public void ChangeState(State newState)
        {
            curState.Exit();
            curState = newState;
            curState.Enter();
        }
    }
}