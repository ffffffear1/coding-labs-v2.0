﻿﻿namespace StateMachine.StateMachineScripts
{
    public class State
    {
        protected StateMachine stateMachine;
        protected Character character;

        public State(StateMachine stateMachine, Character character)
        {
            this.stateMachine = stateMachine;
            this.character = character;
        }

        public virtual void Enter()
        {
        }

        public virtual void HandleInput()
        {
        }

        public virtual void LogicUpdate()
        {
        }

        public virtual void PhysicsUpdate()
        {
        }

        public virtual void Exit()
        {
        }
    }
}
