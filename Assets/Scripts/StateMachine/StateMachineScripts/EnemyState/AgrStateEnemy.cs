﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace StateMachine.StateMachineScripts.EnemyState
{
    public class AgrStateEnemy : State
    {
        public AgrStateEnemy(StateMachine stateMachine, Character character) : base(stateMachine, character) { }
        
        public override void Enter()
        {
            base.Enter();
            character.SetAnimationBool("isRun", true);
        }
        
        public override void HandleInput()
        {
            base.HandleInput();
            character.target = GameObject.FindGameObjectWithTag("Player").transform.position;
            character.Move(character.target);
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            
            if (Vector3.Distance(character.transform.position, character.target) < 1f)
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            else if (Vector3.Distance(character.transform.position, character.target) > 20f)
                stateMachine.ChangeState(character.idle);
        }

        public override void Exit()
        {
            base.Exit();
            character.SetAnimationBool("isRun", false);
        }  
    }
}