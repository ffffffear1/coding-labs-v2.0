﻿using UnityEngine;

namespace StateMachine.StateMachineScripts.EnemyState
{
    public class IdleEnemyState : State
    {
        public Vector3[] pathPoints = 
                {new Vector3(0,0,5), 
                new Vector3(4,0,1), 
                new Vector3(-3,0, -2)}; 
        
        public IdleEnemyState(StateMachine stateMachine, Character character) : base(stateMachine, character) { }
        
        public override void Enter()
        {
            base.Enter();
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            
            if (Vector3.Distance(character.transform.position, character.target) < character.distance)
            {
                if (character.RayToScan())
                {
                    character.target = GameObject.FindGameObjectWithTag("Player").transform.position;
                    stateMachine.ChangeState(character.agr);
                    return;
                }
                else { }
            }
            
            character.target = pathPoints[Random.Range(0, pathPoints.Length)];
            stateMachine.ChangeState(character.run);
        }

        public override void Exit()
        {
            base.Exit();
            character.SetAnimationBool("isRun", true);
        }    
    }
}