﻿using UnityEngine;

namespace StateMachine.StateMachineScripts.EnemyState
{

    public class RunStateEnemy : State
    {
        public RunStateEnemy(StateMachine stateMachine, Character character) : base(stateMachine, character) { }

        public override void Enter()
        {
            base.Enter();
        }

        public override void HandleInput()
        {
            base.HandleInput();
            
            if (Vector3.Distance(character.transform.position, character.target) < character.distance)
            {
                if (character.RayToScan())
                {
                    character.target = GameObject.FindGameObjectWithTag("Player").transform.position;
                    stateMachine.ChangeState(character.agr);
                }
                else { }
            }
            character.Move(character.target);
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (Vector3.Distance(character.transform.position, character.target) < 0.5f)
                stateMachine.ChangeState(character.idle);
        }

        public override void Exit()
        {
            base.Exit();
            character.SetAnimationBool("isRun", false);
        }
    }

}
    
