﻿using StateMachine.StateMachineScripts;
using UnityEngine;
using UnityEngine.AI;

namespace StateMachine
{
    public class Character : MonoBehaviour
    {
        public StateMachineScripts.StateMachine movementSM;
        public State idle;
        public State run;
        public State agr;

        public Vector3 target;
        public NavMeshAgent agent;
        public Animator anim;

        public int rays = 10;
        public int distance = 20;
        public float angle = 60;

        public virtual void Move(Vector3 point)
        {
        }

        public virtual void SetAnimationBool(string param, bool value)
        {
            anim.SetBool(param, value);
        }

        private bool GetRaycast(Vector3 dir)
        {
            var result = false;
            var hit = new RaycastHit();
            var pos = transform.position + new Vector3(0, 1.5f, 0);

            if (Physics.Raycast(pos, dir, out hit, distance))
            {
                result = true;
                Debug.DrawLine(pos, hit.point, Color.green);
            }
            else
                Debug.DrawRay(pos, dir * distance, Color.red);

            return result;
        }

        public bool RayToScan()
        {
            var result = false;
            var a = false;
            var b = false;
            float j = 0;

            for (int i = 0; i < rays; i++)
            {
                var x = Mathf.Sin(j);
                var y = Mathf.Cos(j);

                j += angle * Mathf.Deg2Rad / rays;

                Vector3 dir = transform.TransformDirection(new Vector3(x, 0, y));
                if (GetRaycast(dir)) a = true;

                if (x != 0)
                {
                    dir = transform.TransformDirection(new Vector3(-x, 0, y));
                    if (GetRaycast(dir)) b = true;
                }
            }

            if (a || b) result = true;
            return result;
        }
    }
}