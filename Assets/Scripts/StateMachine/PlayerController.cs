﻿using StateMachine.StateMachineScripts.PlayerState;
using UnityEngine;
using UnityEngine.AI;

namespace StateMachine
{
    public class PlayerController : Character
    {
        private void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            anim = GetComponent<Animator>();

            movementSM = new StateMachineScripts.StateMachine();

            idle = new IdleState(movementSM, this);
            run = new RunStates(movementSM, this);

            movementSM.Init(idle);

        }

        private void Update()
        {
            movementSM.curState.HandleInput();

            movementSM.curState.LogicUpdate();
        }

        public override void Move(Vector3 point)
        {
            agent.SetDestination(point);
        }

        public override void SetAnimationBool(string param, bool value)
        {
            base.SetAnimationBool(param, value);
            anim.SetBool(param, value);
        }
    }
}