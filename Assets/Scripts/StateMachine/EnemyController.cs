﻿using StateMachine.StateMachineScripts;
using StateMachine.StateMachineScripts.EnemyState;
using UnityEngine;
using UnityEngine.AI;

namespace StateMachine
{
    public class EnemyController : Character
    {
        private void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            anim = GetComponent<Animator>();

            movementSM = new StateMachineScripts.StateMachine();

            idle = new IdleEnemyState(movementSM, this);
            run = new RunStateEnemy(movementSM, this);
            agr = new AgrStateEnemy(movementSM, this);

            movementSM.Init(idle);

        }

        private void Update()
        {
            movementSM.curState.HandleInput();
            movementSM.curState.LogicUpdate();
        }

        public override void Move(Vector3 point)
        {
            agent.SetDestination(point);
        }

        public override void SetAnimationBool(string param, bool value)
        {
            base.SetAnimationBool(param, value);
            anim.SetBool(param, value);
        }
    }
}
