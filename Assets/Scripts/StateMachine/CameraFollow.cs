﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;
    private void Update() => transform.position = new Vector3(target.position.x + 2.5f, transform.position.y, target.position.z - 2.5f);
}
