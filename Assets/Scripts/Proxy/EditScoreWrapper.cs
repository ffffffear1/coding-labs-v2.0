﻿using UnityEngine;

namespace Proxy
{
    public class EditScoreWrapper : IGameChanger
    {
        private GameInfo gameInfo;

        public EditScoreWrapper(GameInfo gameInfo)
        {
            this.gameInfo = gameInfo;
        }

        private void ChangeScore(int value) => gameInfo.ChangeScore(value);
        
        public void CompleteLvl()
        {
            gameInfo.CompleteLvl();
            ChangeScore(gameInfo.CurrentLvl * 100);
            Debug.Log(gameInfo.Score);
        }
    }
}