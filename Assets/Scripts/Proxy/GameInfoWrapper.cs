﻿using UnityEngine;

namespace Proxy
{
    public class GameInfoWrapper : IGameChanger
    {
        private GameInfo gameInfo;
        public GameInfoWrapper(GameInfo gameInfo)
        {
            this.gameInfo = gameInfo;
        }

        public void CompleteLvl()
        {
            gameInfo.CompleteLvl();
            Debug.Log($"Текущий уровень: {gameInfo.CurrentLvl}");
        }
    }
}