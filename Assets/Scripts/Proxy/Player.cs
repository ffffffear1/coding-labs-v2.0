﻿using UnityEngine;

namespace Proxy
{
    public class Player : MonoBehaviour
    {
        private IGameChanger Session;
        private GameInfo gameInfo;

        private void Start()
        {
            gameInfo = new GameInfo();
            
            Session = new GameInfoWrapper(gameInfo);
            CompleteLvl();
            
            Session = new EditScoreWrapper(gameInfo);
            CompleteLvl();
        }

        private void CompleteLvl() => Session.CompleteLvl();
    }
}