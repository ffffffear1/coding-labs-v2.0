﻿using Proxy;

public class GameInfo : IGameChanger
{
    public int CurrentLvl;
    public int Score;
    
    public void ChangeScore(int value) => Score += value;
    public void CompleteLvl() => CurrentLvl++;
}
