﻿using UnityEngine;

namespace TemplateMethod
{
    public class App : MonoBehaviour
    {
        private OverridenA overridenA;
        private OverridenB overridenB;
        
        private void Start()
        {
            overridenA = new OverridenA();
            Debug.Log(overridenA.TemplateMethod("1", 2, 3, 4));
            
            overridenB = new OverridenB();
            Debug.Log(overridenB.TemplateMethod("aaa", 2, 3, 6));
        }
    }
}