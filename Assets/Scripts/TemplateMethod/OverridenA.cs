﻿namespace TemplateMethod
{
    public class OverridenA : AbstractClass
    {
        public override int AbstractA(string s)
        {
            return s.Length;
        }

        public override int AbstractB()
        {
            return 42;
        }
    }
}