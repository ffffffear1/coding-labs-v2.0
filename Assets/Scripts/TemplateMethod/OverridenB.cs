﻿using UnityEngine;

namespace TemplateMethod
{
    public class OverridenB : AbstractClass
    {
        string vowelsRus = "АаЕеЁёИиОоУуЫыЭэЮюЯя";
        string vowelsEng = "AaEeIiUuYy";
        
        public override int AbstractA(string s)
        {
            var result = 0;
            
            foreach (char c in s)
            {
                if (vowelsRus.Contains(c.ToString()))
                    result++;
                else if (vowelsEng.Contains(c.ToString()))
                    result++;
            }
  
            return result;
        }

        public override int AbstractB()
        {
            return Random.Range(1, 3);
        }

        public override int VirtualA(int a, int b)
        { 
            return a * b;
        }
        
        public override int VirtualB(int c)
        {
            if (c < 7) return c;
            return c % 7;
        }
    }
}