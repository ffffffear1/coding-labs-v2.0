﻿using System;

namespace TemplateMethod
{
    public abstract class AbstractClass
    {
        public int TemplateMethod(string s, int a, int b, int c)
        {
            return (int) Math.Pow(AbstractA(s), AbstractB()) + (VirtualA(a, b) * VirtualB(c));
        }

        public abstract int AbstractA(string s);
        public abstract int AbstractB();

        public virtual int VirtualA(int a, int b)
        {
            return a + b;
        }

        public virtual int VirtualB(int c)
        {
            return (int) Math.Pow(c, 2);
        }

    }
}