﻿using System.Linq;
using UnityEngine;

namespace CompositeAndFacade.Facade
{
    public class Cook : MonoBehaviour
    {
        protected Knife knife;
        protected Pan pan;
        protected Pot pot;

        private void Start()
        {
            knife = new Knife();
            pan = new Pan();
            pot = new Pot();

            CookSoup();
            CookSandwich();
        }

        private Ingredient CookSandwich()
        {
            SimpleIngedient mazik = new SimpleIngedient("Mazik", 10, 200, 50);
            SimpleIngedient kepchyk = new SimpleIngedient("Kepchyk", 7, 100, 40);

            SimpleIngedient hlebDown = new SimpleIngedient("hlebDown", 1, 50, 30);
            
            ComplexIngredient ketchinez = new ComplexIngredient("ketchinez");
            ketchinez.AddIngredient(mazik);
            ketchinez.AddIngredient(kepchyk);
            
            SimpleIngedient cir = new SimpleIngedient("cir", 2, 150, 120);
            SimpleIngedient kolbasa = new SimpleIngedient("kolbasa", 3, 200, 150);
            SimpleIngedient hlebUp = new SimpleIngedient("hlebUp", 1, 50, 30);
            
            pan.Fry(hlebDown, 2);
            pan.Fry(hlebUp, 2);
            
            ComplexIngredient sandwich = new ComplexIngredient("sandwich");

            sandwich.AddIngredient(hlebDown);
            sandwich.AddIngredient(ketchinez);
            sandwich.AddIngredient(cir);
            sandwich.AddIngredient(kolbasa);
            sandwich.AddIngredient(hlebUp);

            return sandwich;
        }

        private Ingredient CookSoup()
        {
            ComplexIngredient bulon = new ComplexIngredient("bulon");
            
            bulon.AddIngredient(new SimpleIngedient("govyadina", 400, 200, 400));
            bulon.AddIngredient(new SimpleIngedient("voda", 3000, 0, 0));
            bulon.AddIngredient(new SimpleIngedient("lavroviy list", 3, 0, 10));
            bulon.AddIngredient(new SimpleIngedient("sol", 1, 1, 1));
            bulon.AddIngredient(new SimpleIngedient("perec", 1, 1, 1));
            
            pot.Boil(bulon, 100);
            
            bulon.RemoveIngredient(bulon.ingredients.First(name => name.GetName().Equals("lavroviy list")));

            ComplexIngredient zajarka = new ComplexIngredient("zajarka");
            
            zajarka.AddIngredient(new SimpleIngedient("morkov", 100, 20, 20));
            zajarka.AddIngredient(new SimpleIngedient("lyk", 150, 30, 255));
            
            knife.Cut(zajarka);
            pan.Fry(zajarka, 2);
            
            bulon.AddIngredient(zajarka);

            SimpleIngedient kartoshka = new SimpleIngedient("kartoshka", 300, 50, 50);
            knife.Cut(kartoshka);
            
            bulon.AddIngredient(zajarka);
            bulon.AddIngredient(kartoshka);
            
            pot.Boil(bulon, 30);
            pan.Fry(bulon, 30);
            
            return bulon;
        }
    }
}