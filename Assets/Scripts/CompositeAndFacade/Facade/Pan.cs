﻿using UnityEngine;

namespace CompositeAndFacade.Facade
{
    public class Pan
    {
        public void Fry(Ingredient ingredient, float time)
        {
            ingredient.Fry();
            Debug.Log($"{time} минут");
        }
    }
}