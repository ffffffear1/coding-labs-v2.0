﻿namespace CompositeAndFacade.Facade
{
    public class Knife
    {
        public void Cut(Ingredient ingredient)
        {
            ingredient.Cut();
        }
    }
}