﻿using UnityEngine;

namespace CompositeAndFacade.Facade
{
    public class Pot
    {
        public void Boil(Ingredient ingredient, float time)
        {
            ingredient.Boil();
            Debug.Log($"{time} минут");
        }
    }
}