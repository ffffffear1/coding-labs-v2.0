﻿using UnityEngine;

namespace CompositeAndFacade
{
    public class SimpleIngedient : Ingredient
    {
        private string name;
        private float weight;
        private float calorieContent;
        private float cost;
        
        public SimpleIngedient(string name, float weight, float calorieContent, float cost) : base(name, weight, calorieContent, cost)
        {
            this.name = name;
            this.weight = weight;
            this.calorieContent = calorieContent;
            this.cost = cost;
        }

        public override float GetTotalCost() { return weight*cost; }
        public override float GetTotalWeight() { return weight; }
        public override float GetTotalCalories() { return calorieContent*weight; }
        public override void Cut() => Debug.Log($"Режем {name}");
        public override void Boil() => Debug.Log($"Варим {name}");
        public override void Fry() => Debug.Log($"Жарим {name}");
    }
}