﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CompositeAndFacade 
{
    public class ComplexIngredient : Ingredient
    {
        public List<Ingredient> ingredients = new List<Ingredient>();
        
        private string name;
        
        public ComplexIngredient(string name)
        {
            this.name = name;
        }

        public override void AddIngredient(Ingredient i) =>  ingredients.Add(i);
        public override void RemoveIngredient(Ingredient i) => ingredients.Remove(i);
        public override float GetTotalCost() { return ingredients.Sum(ingredient => ingredient.GetTotalCost()); }
        public override float GetTotalWeight() { return ingredients.Sum(ingredient => ingredient.GetTotalWeight()); }
        public override float GetTotalCalories() { return ingredients.Sum(ingredient => ingredient.GetTotalCalories()); }
        public override void Cut() => Debug.Log($"Режем {name}");
        public override void Boil() => Debug.Log($"Варим {name}");
        public override void Fry() => Debug.Log($"Жарим {name}");
    }
}