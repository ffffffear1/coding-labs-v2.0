﻿using UnityEngine;

namespace CompositeAndFacade
{
    public class Sandwich : MonoBehaviour
    {
        private void Start()
        {
            SimpleIngedient mazik = new SimpleIngedient("Mazik", 10, 200, 50);
            SimpleIngedient kepchyk = new SimpleIngedient("Kepchyk", 7, 100, 40);
            
            // List<Ingredient> ketchinezIngredients = new List<Ingredient>();
            // ketchinezIngredients.Add(mazik);
            // ketchinezIngredients.Add(kepchyk);
            
            SimpleIngedient hlebDown = new SimpleIngedient("hlebDown", 1, 50, 30);
            
            ComplexIngredient ketchinez = new ComplexIngredient("ketchinez");
            ketchinez.AddIngredient(mazik);
            ketchinez.AddIngredient(kepchyk);
            
            SimpleIngedient cir = new SimpleIngedient("cir", 2, 150, 120);
            SimpleIngedient kolbasa = new SimpleIngedient("kolbasa", 3, 200, 150);
            SimpleIngedient hlebUp = new SimpleIngedient("hlebUp", 1, 50, 30);
            
            hlebDown.Fry();
            hlebUp.Fry();
            
            // List<Ingredient> sandwichIngredients = new List<Ingredient>();
            
            ComplexIngredient sandwich = new ComplexIngredient("sandwich");

            sandwich.AddIngredient(hlebDown);
            sandwich.AddIngredient(ketchinez);
            sandwich.AddIngredient(cir);
            sandwich.AddIngredient(kolbasa);
            sandwich.AddIngredient(hlebUp);

            Debug.Log($"Цена бутера: {sandwich.GetTotalCost()}");
            Debug.Log($"Вес бутера: {sandwich.GetTotalWeight()}");
            Debug.Log($"Калорийность бутера: {sandwich.GetTotalCalories()}");
        }
    }
}