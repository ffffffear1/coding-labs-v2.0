﻿namespace CompositeAndFacade
{
    
    public class Ingredient
    {
        private string name;
        private float weight;
        private float calorieContent;
        private float cost;

        public Ingredient(string name, float weight, float calorieContent, float cost)
        {
            this.name = name;
            this.weight = weight;
            this.calorieContent = calorieContent;
            this.cost = cost;
        }

        protected Ingredient() { }
        public virtual void Cut() { }
        public virtual void Boil() { }
        public virtual void Fry() { }
        public virtual float GetTotalCost() { return cost; }
        public virtual float GetTotalWeight() { return weight; }
        public virtual float GetTotalCalories() { return calorieContent; }
        public virtual void AddIngredient(Ingredient i) { }
        public virtual void RemoveIngredient(Ingredient i) { }

        public string GetName()
        {
            return name;
        }
    }
}